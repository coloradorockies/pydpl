**pyDPL**

A Python package to interact with the MLB Draft Prospect Link API.

http://prospectlink-api.mlb.com/swagger-ui.html#/

---

## Requirements

1. Install [Python 3](https://www.python.org/)
2. Install [pip](https://pip.pypa.io/en/stable/installing/)
3. Gain access to the Rockies pyDPL repository on bitbucket

---

## Installation

The package can be installed via 'pip' using [either git+https, or git+ssh](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)

1. Install via https and account credentials (requires user to input credentials to install)
	```
	pip install git+https://bitbucket.org/coloradorockies/pydpl.git
	```
2. Install via ssh if ssh keys are configured for your bitbucket account and computer
	```
	pip install git+ssh://git@bitbucket.org/coloradorockies/pydpl.git
	```

To upgrade, run either of the previous commands with the ```--upgrade``` option

---

## Usage / Getting Started

Here is a super simple example
```python
# Import the package
import pyDPL

# Create a link
link= pyDPL.DraftProspectLink('first.last@rockies.com','mySecretPassword')

# Hit Questionnaire Endpoint (for easy example)
print(link.Questionnaires())

# Close the Link (REQUIRED - Otherwise the script with not exit and keep the connection open)
link.close()
```

---

## API

[Official Draft Prospect Link Swagger Documentation](http://prospectlink-api.mlb.com/swagger-ui.html#/)

Updated After Date - Format: MM/DD/YYYY hh:mm a. 
	Example: updatedAfter=08/08/2017%2011:25%20AM


##### Contract Controller
```python

# /prospects/{id}/contacts
link.ProspectContacts(id)

# /prospects/contacts
link.ProspectContactsUpdatedAfter(page=0, size=50, updatedAfter=None)

# /prospects/ebisid/{ebisId}/contacts
link.ProspectContactsByEbisId(id)

```

##### DPIM External Controller
```python

# /dpim/matches
link.dpimMatches(matchedAfter=None, page=0, size=1000)

# /dpim/systems
link.dpimSystems(importStartedAfter=None)

```

##### Draft Submission Controller
```python

# /prospects/draftsubmissions
link.DraftSubmissions(page=0, size=50, updatedAfter=None)

```


##### Invitation Controller
```python

# /invitations/{id}
link.Invitation(id)

# /invitations
link.InvitationUpdatedAfter(page=0, size=100, updatedAfter=None)

```


##### Medical Request Controller
```python

# /prospects/{id}/medical/requests
link.MedicalRequest(id)

# /prospects/medical/requests
link.MedicalRequestUpdatedAfter(page=0, size=100, updatedAfter=None)

# /invitation/requests
link.InvitationRequests()

# /invitation/request/{invitationRequestId}
link.InvitationRequestById(id)

```


##### Prospect Controller
```python

# /prospects/{id}
link.Prospect(id)

# /prospects
link.ProspectUpdatedAfter(page=0, size=50, sort='id', updatedAfter=None)

# /prospects/duplicateremediation
link.ProspectDuplicates()

# /prospects/ebisid/{ebisId}
link.ProspectByEbisId(id)

```


##### Questionnaire Controller
```python

# /prospects/{id}/questionnaires
link.QuestionnaireForProspect(id)

# /prospects/questionnaires
link.QuestionnaireUpdatedAfter(page=0, size=50, updatedAfter=None)

# /questionnaires
link.Questionnaires()

# /prospects/ebisid/{ebisId}/questionnaires
link.QuestionnaireForProspectByEbisId(id)

```

##### Shared Link Controller
```python

# /prospects/{prospectId}/external/sharedlinks
link.ProspectExternalSharedLink(id)

# /prospects/{prospectId}/media/sharedlinks
link.ProspectMediaSharedLink(id)

# /prospects/{prospectId}/medical/sharedlinks
link.ProspectMedicalSharedLink(id)

# /prospects/external/sharedlinks
link.ExternalSharedLinkUpdatedAfter(addedToOrgSince=None ,page=0, size=100, updatedAfter=None)

# /prospects/media/sharedlinks
link.MediaSharedLinkUpdatedAfter(page=0, size=100, updatedAfter=None)

# /prospects/medical/sharedlinks
link.MedicalSharedLinkUpdatedAfter(page=0, size=100, updatedAfter=None)

```


##### School History Controller
```python

# /prospects/{id}/schools
link.ProspectSchool(id)

# /prospects/schools
link.ProspectSchoolUpdatedAfter(page=0, size=100, updatedAfter=None)

# /prospects/ebisid/{ebisId}/schools
link.ProspectSchoolByEbisId(id)

```


##### Task Controller
```python

# /prospects/{id}/tasks
link.ProspectTask(id)

# /prospects/tasks
link.ProspectTaskUpdatedAfter(page=0, size=100, updatedAfter=None)

# /prospects/ebisid/{id}/tasks
link.ProspectTaskByEbisId(id)

# /prospects/{prospectId}/tasks/{taskId}
link.ProspectTaskByTaskId(prospectId, taskId)

# /tasks
link.TaskList()

```


##### Social Media Controller
```python

# /prospects/{id}/socialMedia
link.ProspectSocialMedia(id)

# /prospects/socialMedia
link.AllSocialMedia(page=0, size=50)

```


##### Tags Controller
```python

# /tags/{id}/prospects
link.ProspectsByTag(id, page=0, size=100, sort='id')

# /tags
link.Tags()

```

##### User Controller
```python

# /users
link.ClubUsers()

```