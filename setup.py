
from setuptools import setup, find_packages

setup(
	name='pyDPL',
	version='1.3.2',
	packages=find_packages(),
	description='Python Library for connection to the MLB Draft Prospect Link API',
	author='Abi Sislo',
	author_email='abi.sislo@rockies.com',
	install_requires=['requests>=2.19']
)