# invitation-controller
# Draft Prospect Link Invitation Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /invitations/{id}
# getInvitation
def Invitation(self, id):
	path = "/invitations/%s" % id
	return self._apiRequest(path)

# /invitations
# getInvitations
def InvitationUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/invitations"
	return self._apiRequest(path, params)

# /invitation/requests
# getInvitationRequests
def InvitationRequests(self):
	path = "/invitation/requests"
	return self._apiRequest(path)

# /invitation/request/{invitationRequestId}
# getInvitationRequest
def InvitationRequestById(self, id):
	path = "/invitation/request/%s" % id
	return self._apiRequest(path)