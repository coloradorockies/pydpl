# Social-Media-Controller
# Draft Prospect Link Social Media Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/socialMedia
# getSocialMediaForProspect
def ProspectSocialMedia(self, id):
	path = "/prospects/%s/socialMedia" % id
	return self._apiRequest(path)

# /prospects/socialMedia
# getSocialMediaForVisibileProspects
def AllSocialMedia(self, page=0, size=50):
	# Set up request params
	params = {'page': page, 'size': size}

	# Send the request
	path = "/prospects/socialMedia"
	return self._apiRequest(path, params)