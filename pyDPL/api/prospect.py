# prospect-controller
# Draft Prospect Link Prospect Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}
# getProspect
def Prospect(self, id):
	path = "/prospects/%s" % id
	return self._apiRequest(path)

# /prospects/ebisid/{ebisId}
# getProspectByEbisId
def ProspectByEbisId(self, id):
	path = "/prospects/ebisid/%s" % id
	return self._apiRequest(path)

# /prospects
# getProspects
def ProspectUpdatedAfter(self, page=0, size=50, sort='id', updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'sort':sort, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects"
	return self._apiRequest(path, params)

# /prospects/duplicateremediation
# getProspectDuplicateInfo
def ProspectDuplicates(self):
	# Send the request
	path = "/prospects/duplicateremediation"
	return self._apiRequest(path)