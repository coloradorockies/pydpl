# DPIM-External-Controller
# DPIM External Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /dpim/matches
# getPlayerMatches
def dpimMatches(self, matchedAfter=None, page=0, size=1000):
	# Set up request params
	if matchedAfter is None:
		matchedAfter= '1900-01-01'

	params = {'matchedAfter': matchedAfter, 'page': page, 'size': size}

	# Send the request
	path = "/dpim/matches"
	return self._apiRequest(path, params)

# /dpim/systems
# getExternalSystems
def dpimSystems(self, importStartedAfter=None):
	# Set up request params
	if importStartedAfter is None:
		importStartedAfter= '1900-01-01'

	params = {'importStartedAfter': importStartedAfter}

	# Send the request
	path = "/dpim/systems"
	return self._apiRequest(path, params)