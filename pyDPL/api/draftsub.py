# Contact-Controller
# Draft Prospect Link Draft Submission Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/draftsubmissions
# getAllProspectContacts
def DraftSubmissions(self, page=0, size=50, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/draftsubmissions"
	return self._apiRequest(path, params)