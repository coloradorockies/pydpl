# school-history-controller
# Draft Prospect Link School History Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/schools
# getSchoolHistory
def ProspectSchool(self, id):
	path = "/prospects/%s/schools" % id
	return self._apiRequest(path)

# /prospects/ebisid/{ebisId}/schools
# getSchoolHistoryByEbisId
def ProspectSchoolByEbisId(self, id):
	path = "/prospects/ebisid/%s/schools" % id
	return self._apiRequest(path)

# /prospects/schools
# getAllProspectSchoolHistory
def ProspectSchoolUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/schools"
	return self._apiRequest(path, params)