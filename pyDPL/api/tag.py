# Tags-Controller
# Draft Prospect Link Tags Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /tags/{id}/prospects
# getProspectsByTag
def ProspectsByTag(self, id, page=0, size=100, sort='id'):
	path = "/tags/%s/prospects" % id	#ID here should be the Tag ID not the Prospect ID
	params = {'page': page, 'size': size, 'sort': sort}
	return self._apiRequest(path)

# /tags
# getOrgTags
def Tags(self):
	# Send the request
	path = "/tags"
	return self._apiRequest(path)