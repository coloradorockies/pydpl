# task-controller
# Draft Prospect Link Task Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/tasks
# getProspectTasks
def ProspectTask(self, id):
	path = "/prospects/%s/tasks" % id
	return self._apiRequest(path)

# /prospects/ebisid/{id}/tasks
# getTasksByEbisId
def ProspectTaskByEbisId(self, id):
	path = "/prospects/ebisid/%s/tasks" % id
	return self._apiRequest(path)

# /prospects/{prospectId}/tasks/{taskId}
# downloadProspectTask
def ProspectTaskByTaskId(self, prospectId, taskId):
	path = "/prospects/%s/tasks/%s" % prospectId,taskId
	return self._apiRequest(path)

# /prospects/tasks
# getAllProspectTasks
def ProspectTaskUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/tasks"
	return self._apiRequest(path, params)

# /tasks
# getAllTasks
def TaskList(self):
	path = "/tasks"
	return self._apiRequest(path)