# medical-request-controller
# Draft Prospect Link Medical Request Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/medical/requests
# getProspectMedicalRequests
def MedicalRequest(self, id):
	path = "/prospects/%s/medical/requests" % id
	return self._apiRequest(path)

# /prospects/ebisid/{ebisId}/medical/requests
# getProspectMedicalRequests
def MedicalRequestByEbisId(self, id):
	path = "/prospects/ebisid/%s/medical/requests" % id
	return self._apiRequest(path)

# /prospects/medical/requests
# getProspectMedicalRequestsByEbisId
def MedicalRequestUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/medical/requests"
	return self._apiRequest(path, params)