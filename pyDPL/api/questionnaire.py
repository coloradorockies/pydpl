# questionnaire-controller
# Draft Prospect Link Questionnaire Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/questionnaires
# getProspectQuestionnaires
def QuestionnaireForProspect(self, id):
	path = "/prospects/%s/questionnaires" % id
	return self._apiRequest(path)

# /prospects/ebisid/{ebisId}/questionnaires
# getQuestionnairesByEbisId
def QuestionnaireForProspectByEbisId(self, id):
	path = "/prospects/ebisid/%s/questionnaires" % id
	return self._apiRequest(path)

# /prospects/questionnaires
# getQuestionnairesByOrg
def QuestionnaireUpdatedAfter(self, page=0, size=50, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/questionnaires"
	return self._apiRequest(path, params)

# /questionnaires
# getQuestionnaires
def Questionnaires(self):
	# Send the request
	path = "/questionnaires"
	return self._apiRequest(path)