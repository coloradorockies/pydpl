# shared-link-controller
# Draft Prospect Link Shared Link Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{prospectId}/external/sharedlinks
# getProspectTasks
def ProspectExternalSharedLink(self, id):
	path = "/prospects/%s/external/sharedlinks" % id
	return self._apiRequest(path)

# /prospects/{prospectId}/media/sharedlinks
# getProspectTasks
def ProspectMediaSharedLink(self, id):
	path = "/prospects/%s/media/sharedlinks" % id
	return self._apiRequest(path)

# /prospects/{prospectId}/medical/sharedlinks
# getProspectTasks
def ProspectMedicalSharedLink(self, id):
	path = "/prospects/%s/medical/sharedlinks" % id
	return self._apiRequest(path)

# /prospects/external/sharedlinks
# getAllProspectTasks
def ExternalSharedLinkUpdatedAfter(self, addedToOrgSince=None ,page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		updatedAfter= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 
	if addedToOrgSince is None:
		addedToOrgSince= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter, 'addedToOrgSince': addedToOrgSince}

	# Send the request
	path = "/prospects/external/sharedlinks"
	return self._apiRequest(path, params)

# /prospects/media/sharedlinks
# getAllProspectTasks
def MediaSharedLinkUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		updatedAfter= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 
	if addedToOrgSince is None:
		addedToOrgSince= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter, 'addedToOrgSince': addedToOrgSince}

	# Send the request
	path = "/prospects/media/sharedlinks"
	return self._apiRequest(path, params)

# /prospects/medical/sharedlinks
# getAllProspectTasks
def MedicalSharedLinkUpdatedAfter(self, page=0, size=100, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		updatedAfter= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 
	if addedToOrgSince is None:
		addedToOrgSince= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter, 'addedToOrgSince': addedToOrgSince}

	# Send the request
	path = "/prospects/medical/sharedlinks"
	return self._apiRequest(path, params)
