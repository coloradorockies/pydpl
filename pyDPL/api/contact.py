# Contact-Controller
# Draft Prospect Link Contact Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /prospects/{id}/contacts
# getProspectContacts
def ProspectContacts(self, id):
	path = "/prospects/%s/contacts" % id
	return self._apiRequest(path)

# /prospects/contacts
# getAllProspectContacts
def ProspectContactsUpdatedAfter(self, page=0, size=50, updatedAfter=None):
	# Set up request params
	if updatedAfter is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p') 

	params = {'page': page, 'size': size, 'updatedAfter': updatedAfter}

	# Send the request
	path = "/prospects/contacts"
	return self._apiRequest(path, params)

# /prospects/ebisid/{ebisId}/contacts
# getProspectContactsByEbisId
def ProspectContactsByEbisId(self, id):
	path = "/prospects/ebisid/%s/contacts" % id
	return self._apiRequest(path)