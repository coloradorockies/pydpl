# User-Controller
# Draft Prospect Link User Controller
# https://draftprospectlinkapi-jx-prod-draftprospectlinkapi.cl1-eks.prod.aws-us-east-1.infra.mlbinfra.net/swagger-ui.html#/

import datetime

# /users
# getClubUsers
def ClubUsers(self):
	# Send the request
	path = "/users"
	return self._apiRequest(path)