import requests

DPL_AUTH= 'https://prospectlink-api.mlb.com/access'
DPL_ROOT= 'https://prospectlink-api.mlb.com/api/v1'


class DraftProspectLink():

	def __init__(self, username, password, debug=False):
		self._username= username
		self._password= password
		self._debugLog= debug
		self._login()

	# Deprecated
	def close(self):
		return

	def _debug(self, text):
		if self._debugLog:
			print(text)

	def _authHeader(self):
		return "Bearer %s" % self._token

	def _login(self):
		self._debug('DPL._login: loging in')
		headers= {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Accept': 'application/json'
		}
		data= {
			'username': self._username,
			'password': self._password
		}
		r= requests.post(DPL_AUTH, data=data, headers=headers)
		if r.status_code == 200:
			res= r.json()
			self._debug('DPL._login: res: %s' % res)
			self._token= res['access_token']
		else:
			print('DPL: Unable to authenticate with the given credentials')
			self._token= ''
			raise AuthenticationError('%s: %s' % (r.status_code, r.reason))

	def _apiRequest(self, path, params=None):
		url= '%s%s' % (DPL_ROOT, path)
		headers= {
			'Accept': 'application/json',
			'Authorization': self._authHeader()
		}
		r= requests.get(url, params=params, headers=headers)
		if r.status_code == 200:
			return r.json()
		elif r.status_code == 401:
			self._debug('Got Expired Token. Refreshing Token and trying again')
			self._login()
			self._apiRequest(path, params)
		else:
			raise ApiRequestError('%s: %s:' % (r.status_code, r.text))

	# ### API Endpoints ###

	# Contract Controller
	from .api.contact import ProspectContacts
	from .api.contact import ProspectContactsUpdatedAfter
	from .api.contact import ProspectContactsByEbisId

	# DPIM Controller
	from .api.dpim import dpimMatches
	from .api.dpim import dpimSystems

	# Draft Submission Controller
	from .api.draftsub import DraftSubmissions

	# Invitation Controller
	from .api.invitation import Invitation
	from .api.invitation import InvitationUpdatedAfter
	from .api.invitation import InvitationRequestById

	# Medical Request Controller
	from .api.medrequest import MedicalRequest
	from .api.medrequest import MedicalRequestUpdatedAfter
	from .api.medrequest import MedicalRequestByEbisId

	# Prospect Controller
	from .api.prospect import Prospect
	from .api.prospect import ProspectUpdatedAfter
	from .api.prospect import ProspectDuplicates
	from .api.prospect import ProspectByEbisId

	# Questionnaire Controller
	from .api.questionnaire import QuestionnaireForProspect
	from .api.questionnaire import QuestionnaireUpdatedAfter
	from .api.questionnaire import Questionnaires
	from .api.questionnaire import QuestionnaireForProspectByEbisId

	# School History Controller
	from .api.schoolhist import ProspectSchool
	from .api.schoolhist import ProspectSchoolUpdatedAfter
	from .api.schoolhist import ProspectSchoolByEbisId

	# Shared Link Controller
	from .api.sharedlink import ProspectExternalSharedLink
	from .api.sharedlink import ProspectMediaSharedLink
	from .api.sharedlink import ProspectMedicalSharedLink
	from .api.sharedlink import ExternalSharedLinkUpdatedAfter
	from .api.sharedlink import MediaSharedLinkUpdatedAfter
	from .api.sharedlink import MedicalSharedLinkUpdatedAfter

	# Social Media Controller
	from .api.socialmedia import AllSocialMedia
	from .api.socialmedia import ProspectSocialMedia

	# Tags Controller
	from .api.tag import ProspectsByTag
	from .api.tag import Tags

	# Task Controller
	from .api.task import ProspectTask
	from .api.task import ProspectTaskUpdatedAfter
	from .api.task import TaskList
	from .api.task import ProspectTaskByEbisId
	from .api.task import ProspectTaskByTaskId

	# User Controller
	from .api.user import ClubUsers

class AuthenticationError(Exception):
	pass


class ApiRequestError(Exception):
	pass